print('Hello World!')

import numpy as np
r=float(input('Enter the radius of the circle: '))
def circ(r):
	omtr=np.pi*r*2
	return omtr

TEST=circ(r)
print('The circumference of the circle with radius', r, 'is', TEST)

def circ_opp(r):
	opp=np.pi*r**2
	return opp

TEST_1=circ_opp(r)
print('The surface of the circle with radius', r, 'is', TEST_1)
